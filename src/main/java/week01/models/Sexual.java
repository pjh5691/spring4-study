package week01.models;

public enum Sexual {

	MAN(0), WOMAN(1);
	
	private int kind;
	
	private Sexual(int kind) {
		this.kind = kind;
	}
	
	public int getKind() {
		return this.kind;
	}
}
